#include "VRenderer.h"

VRenderer *VRenderer::m_spRenderer = NULL;

VRenderer *VRenderer::CreateVolumeRenderer(HWND hWnd)
{
	if(m_spRenderer)
		return NULL;

	m_spRenderer = new VRenderer(hWnd);

	return m_spRenderer;
}

VRenderer *VRenderer::GetInstance()
{
	return m_spRenderer;
}

void VRenderer::DisposeVolumeRenderer()
{
	if(m_spRenderer)
	{
		delete m_spRenderer;
		m_spRenderer = NULL;
	}
}

VRenderer::VRenderer(HWND hWnd)
: m_hWnd(hWnd)
, m_pVoxelScene(NULL)
, m_pImage(NULL)
, m_fDistance(300.0f)
{
	RECT rect;
	::GetClientRect(hWnd, &rect);
	m_nScreenWidth = rect.right;
	m_nScreenHeight = rect.bottom;
}

VRenderer::~VRenderer(void)
{
	this->Cleanup();
}

VoxelScene *VRenderer::GetVoxelScene() const
{
	return m_pVoxelScene;
}

void VRenderer::SetCamera(float orbitX, float orbitY, float orbitZ, float distance)
{
	m_matRotX.MakeRotationX(orbitX);
	m_matRotY.MakeRotationY(orbitY);
	m_matRotZ.MakeRotationZ(orbitZ);
	m_fDistance = distance;
}

bool VRenderer::Render(HDC hdc)
{
	if(!hdc)
		return false;

	Matrix matRot = m_matRotX * m_matRotY * m_matRotZ;

	int maxIndex = (m_nScreenHeight - 1) * (m_nScreenWidth - 1);
	// 물체를 바라보는 가상의 화면에서 각 픽셀을 원점으로 하는 모든 광선을 화면에 수직하게 발사
#pragma omp parallel for
	for(int index = 0; index <= maxIndex; ++index)
	{
		int j = index / m_nScreenWidth;
		int i = index - j * m_nScreenWidth;

		float x = (float)i - ((float)m_nScreenWidth / 2.0f);
		float y = ((float)m_nScreenHeight / 2.0f) - (float)j;

		Vector3 vStart(x, y, -m_fDistance);
		vStart = vStart.TransformNormal(matRot);

		Vector3 vEnd(x, y, 0.0f);
		vEnd = vEnd.TransformNormal(matRot);

		//Ray ray(Vector3(x, y, -300.0), Vector3(x, y, 0.0));*/
		Ray ray(vStart, vEnd);
		Vector3 C = this->CastRay(ray);

		// 이미지 배열에 결과 색상 저장
		m_pImage[(j * m_nScreenWidth + i) * 3 + 2] = (unsigned char)(255 * min(C.x, 1.0f));
		m_pImage[(j * m_nScreenWidth + i) * 3 + 1] = (unsigned char)(255 * min(C.y, 1.0f));
		m_pImage[(j * m_nScreenWidth + i) * 3 + 0] = (unsigned char)(255 * min(C.z, 1.0f));
	}

	// 이미지 배열을 화면에 비트맵으로 그림
	BITMAPINFOHEADER bmiHeader;
	bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bmiHeader.biWidth = m_nScreenWidth;
	bmiHeader.biHeight = m_nScreenHeight;
	bmiHeader.biPlanes = 1;
	bmiHeader.biBitCount = 24;
	bmiHeader.biCompression = BI_RGB;
	bmiHeader.biSizeImage = 0;
	bmiHeader.biXPelsPerMeter = 0;
	bmiHeader.biYPelsPerMeter = 0;
	bmiHeader.biClrUsed = 0;
	bmiHeader.biClrImportant = 0;
	StretchDIBits(hdc, 0, 0, m_nScreenWidth, m_nScreenHeight, 0, 0, m_nScreenWidth, m_nScreenHeight, m_pImage, (LPBITMAPINFO)(&bmiHeader), DIB_RGB_COLORS, SRCCOPY);

	return true;
}

bool VRenderer::Initialize(const char *volumeFileName, int volumeWidth, int volumeHeight, int volumeDepth)
{
	if(m_pVoxelScene)
		return false;

	FILE *pF = NULL;
	if(fopen_s(&pF, volumeFileName, "rb") != 0)
		return false;

	// 파일로부터 데이터 받을 준비(메모리 할당)
	unsigned char *pDensityData = new unsigned char[volumeWidth * volumeHeight * volumeDepth];

	// 파일로부터 배열로 밀도값 읽음
	fread(pDensityData, sizeof(unsigned char), volumeWidth * volumeHeight * volumeDepth, pF);

	fclose(pF);

	m_pVoxelScene = new VoxelScene(volumeWidth, volumeHeight, volumeDepth);
	// voxel 계산
	if(!m_pVoxelScene->InitializeVoxelData(pDensityData))
	{
		this->Cleanup();
		return false;
	}

	delete [] pDensityData;

	// 이미지 버퍼 생성(width * height * 3(RGB))
	m_pImage = new unsigned char[m_nScreenWidth * m_nScreenHeight * 3];

	return true;
}

void VRenderer::Cleanup()
{
	if(m_pVoxelScene)
	{
		delete m_pVoxelScene;
		m_pVoxelScene = NULL;
	}

	if(m_pImage)
	{
		delete [] m_pImage;
		m_pImage = NULL;
	}
}

Vector3 VRenderer::CastRay(const Ray &ray)
{
	Color C_out;
	C_out.opacity = 0.0f;
	Vector3 normal;
	float maxT = m_fDistance * 2.0f;
	for(float t = 0; t < maxT; t += 0.5f)
	{
		// 광선의 원점으로부터 t만큼 떨어진 샘플링 좌표
		Vector3 sample(ray.Eval(t));

		// voxel 데이터 내에 존재하는 광원만 색 계산
		if(!m_pVoxelScene->IsInVoxelData(sample))
			continue;

		// 샘플링 좌표에 해당하는 불투명도와 색을, 주위의 voxel 데이터를 참조 및 보간하여 계산(resampling)
		Color C_i = m_pVoxelScene->Resampling(ray, sample, normal);

		// 샘플링할 때 마다 색 누적(composition)
		Color C_in = C_out;
		// I_out = I_in + (1 - a_in) * a_i * C_i
		C_out.color = C_in.color + (1.0f - C_in.opacity) * C_i.opacity * C_i.color;
		// a_out = a_in + a_i * (1 - a_in)
		C_out.opacity = C_in.opacity + C_i.opacity * (1.0f - C_in.opacity);

		// 불투명도가 1이상이 되면 더 이상 샘플링할 필요 없음
		if(C_out.opacity >= 1.0f)
			break;
	}

	return C_out.color;
}
