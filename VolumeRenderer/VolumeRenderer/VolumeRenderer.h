#pragma once

#include "resource.h"
#include "stdafx.h"
#include "VRenderer.h"

#define MAX_LOADSTRING 100
#define RAD_TO_DEG(x) (x * 57.29577951f)
#define DEG_TO_RAD(x) (x * 0.017453293f)

// 전역 변수:
extern HINSTANCE hInst;								// 현재 인스턴스입니다.
extern HWND hWnd;	// 현재 윈도우
extern TCHAR szTitle[MAX_LOADSTRING];					// 제목 표시줄 텍스트입니다.
extern TCHAR szWindowClass[MAX_LOADSTRING];			// 기본 창 클래스 이름입니다.
extern VRenderer *pRenderer;	// 볼륨 렌더러
extern float fOrbitX;	// x축 공전 각
extern float fOrbitY;	// y축 공전 각
extern float fOrbitZ;	// z축 공전 각
extern float fOrbitDistance;	// 화면이 중심으로부터 떨어진 거리
extern unsigned char ucDensityLow;	// 렌더링 허용할 밀도 최소값
extern unsigned char ucDensityHigh;	// 렌더링 허용할 밀도 최대값

// 이 코드 모듈에 들어 있는 함수의 정방향 선언입니다.
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	RenderingOption(HWND, UINT, WPARAM, LPARAM);
bool InitializeVRenderer();
void AcceptRenderingOption();
void CleanupVRenderer();

//
//  함수: MyRegisterClass()
//
//  목적: 창 클래스를 등록합니다.
//
//  설명:
//
//    Windows 95에서 추가된 'RegisterClassEx' 함수보다 먼저
//    해당 코드가 Win32 시스템과 호환되도록
//    하려는 경우에만 이 함수를 사용합니다. 이 함수를 호출해야
//    해당 응용 프로그램에 연결된
//    '올바른 형식의' 작은 아이콘을 가져올 수 있습니다.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_VOLUMERENDERER));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_VOLUMERENDERER);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   함수: InitInstance(HINSTANCE, int)
//
//   목적: 인스턴스 핸들을 저장하고 주 창을 만듭니다.
//
//   설명:
//
//        이 함수를 통해 인스턴스 핸들을 전역 변수에 저장하고
//        주 프로그램 창을 만든 다음 표시합니다.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // 인스턴스 핸들을 전역 변수에 저장합니다.

   hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      100, 100, 600, 600/*CW_USEDEFAULT, 0, CW_USEDEFAULT, 0*/, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

// 정보 대화 상자의 메시지 처리기입니다.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

// 정보 대화 상자의 메시지 처리기입니다.
INT_PTR CALLBACK RenderingOption(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HWND hEdtRotX;
	static HWND hEdtRotY;
	static HWND hEdtRotZ;
	static HWND hEdtDistance;
	static HWND hEdtDensLow;
	static HWND hEdtDensHigh;

	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		{
			hEdtRotX = GetDlgItem(hDlg, IDC_EDT_ROT_X);
			hEdtRotY = GetDlgItem(hDlg, IDC_EDT_ROT_Y);
			hEdtRotZ = GetDlgItem(hDlg, IDC_EDT_ROT_Z);
			hEdtDistance = GetDlgItem(hDlg, IDC_EDT_DIST);
			hEdtDensLow = GetDlgItem(hDlg, IDC_EDT_DENS_LOW);
			hEdtDensHigh = GetDlgItem(hDlg, IDC_EDT_DENS_HIGH);

			wchar_t wcsTemp[MAXCHAR];
			swprintf_s(wcsTemp, MAXCHAR, L"%f", RAD_TO_DEG(fOrbitX));
			SetWindowText(hEdtRotX, wcsTemp);
			swprintf_s(wcsTemp, MAXCHAR, L"%f", RAD_TO_DEG(fOrbitY));
			SetWindowText(hEdtRotY, wcsTemp);
			swprintf_s(wcsTemp, MAXCHAR, L"%f", RAD_TO_DEG(fOrbitZ));
			SetWindowText(hEdtRotZ, wcsTemp);
			swprintf_s(wcsTemp, MAXCHAR, L"%f", fOrbitDistance);
			SetWindowText(hEdtDistance, wcsTemp);
			swprintf_s(wcsTemp, MAXCHAR, L"%u", ucDensityLow);
			SetWindowText(hEdtDensLow, wcsTemp);
			swprintf_s(wcsTemp, MAXCHAR, L"%u", ucDensityHigh);
			SetWindowText(hEdtDensHigh, wcsTemp);
		}
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			{
				wchar_t wcsTemp[MAXCHAR];
				GetWindowText(hEdtRotX, wcsTemp, MAXCHAR);
				fOrbitX = DEG_TO_RAD((float)_wtof(wcsTemp));
				GetWindowText(hEdtRotY, wcsTemp, MAXCHAR);
				fOrbitY = DEG_TO_RAD((float)_wtof(wcsTemp));
				GetWindowText(hEdtRotZ, wcsTemp, MAXCHAR);
				fOrbitZ = DEG_TO_RAD((float)_wtof(wcsTemp));
				GetWindowText(hEdtDistance, wcsTemp, MAXCHAR);
				fOrbitDistance = (float)_wtof(wcsTemp);
				GetWindowText(hEdtDensLow, wcsTemp, MAXCHAR);
				ucDensityLow = (unsigned char)_wtoi(wcsTemp);
				GetWindowText(hEdtDensHigh, wcsTemp, MAXCHAR);
				ucDensityHigh = (unsigned char)_wtoi(wcsTemp);

				AcceptRenderingOption();
				EndDialog(hDlg, LOWORD(wParam));
				return (INT_PTR)TRUE;
			}
			break;
		case IDCANCEL:
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
			break;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
