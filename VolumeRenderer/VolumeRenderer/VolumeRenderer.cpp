// VolumeRenderer.cpp : 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include "VolumeRenderer.h"

#ifdef _DEBUG
#include <crtdbg.h>
#endif

// 전역 변수:
HINSTANCE hInst;								// 현재 인스턴스입니다.
HWND hWnd;	// 현재 윈도우
TCHAR szTitle[MAX_LOADSTRING];					// 제목 표시줄 텍스트입니다.
TCHAR szWindowClass[MAX_LOADSTRING];			// 기본 창 클래스 이름입니다.
VRenderer *pRenderer = NULL;	// 볼륨 렌더러
float fOrbitX = 0.0f;//3.14f * 0.3f;
float fOrbitY = 0.0f;
float fOrbitZ = 0.0f;//3.14f * 0.25f;
float fOrbitDistance = 400.0f;
unsigned char ucDensityLow = 50;	// 렌더링 허용할 밀도 최소값
unsigned char ucDensityHigh = 95;	// 렌더링 허용할 밀도 최대값

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
#ifdef _DEBUG
	_CrtSetDbgFlag(0x21);
#endif

	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: 여기에 코드를 입력합니다.
	MSG msg;
	HACCEL hAccelTable;

	// 전역 문자열을 초기화합니다.
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_VOLUMERENDERER, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// 응용 프로그램 초기화를 수행합니다.
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_VOLUMERENDERER));

	// 기본 메시지 루프입니다.
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  함수: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  목적: 주 창의 메시지를 처리합니다.
//
//  WM_COMMAND	- 응용 프로그램 메뉴를 처리합니다.
//  WM_PAINT	- 주 창을 그립니다.
//  WM_DESTROY	- 종료 메시지를 게시하고 반환합니다.
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_CREATE:
		::hWnd = hWnd;
		// 볼륨 렌더러 초기화
		if(!InitializeVRenderer())
		{
			MessageBox(hWnd, L"Failed to initializing VRenderer.", L"Error", MB_OK);
			CleanupVRenderer();
			PostQuitMessage(EXIT_FAILURE);
		}
		break;
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// 메뉴의 선택 영역을 구문 분석합니다.
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_OPTION:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_OPTION), hWnd, RenderingOption);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: 여기에 그리기 코드를 추가합니다.

		if(!pRenderer->Render(hdc))
		{
			MessageBox(hWnd, L"Failed to rendering the volume data.", L"Error", MB_OK);
			CleanupVRenderer();
			PostQuitMessage(EXIT_FAILURE);
		}

		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		// 볼륨 렌더러 제거
		CleanupVRenderer();

		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

bool InitializeVRenderer()
{
	pRenderer = VRenderer::CreateVolumeRenderer(hWnd);
	if(!pRenderer->Initialize("data/bighead.den", 256, 256, 225))
	//if(!pRenderer->Initialize("data/brain.den", 128, 128, 84))
	{
		return false;
	}

	pRenderer->SetCamera(fOrbitX, fOrbitY, fOrbitZ, fOrbitDistance);

	VoxelScene *pVC = pRenderer->GetVoxelScene();
	pVC->SetDirectionalLight(Vector3(1.0f, 1.0f, 1.0f)
		, Material(Vector3(0.2f, 0.2f, 0.2f)
			, Vector3(0.5f, 0.5f, 0.5f)
			, Vector3(0.5f, 0.5f, 0.5f)));
	pVC->SetVoxelMaterial(Material(Vector3(0.2f, 0.2f, 0.2f)
		, Vector3(0.5f, 0.5f, 0.5f)
		, Vector3(0.5f, 0.5f, 0.5f)));
	// bighead: 살:50, 뼈: 96
	// brain: 살:24
	pVC->SetDensityBound(ucDensityLow, ucDensityHigh);
	pVC->SetScale(2.0f);

	return true;
}

void AcceptRenderingOption()
{
	pRenderer->SetCamera(fOrbitX, fOrbitY, fOrbitZ, fOrbitDistance);
	pRenderer->GetVoxelScene()->SetDensityBound(ucDensityLow, ucDensityHigh);
	InvalidateRect(hWnd, NULL, FALSE);
}

void CleanupVRenderer()
{
	VRenderer::DisposeVolumeRenderer();
}
