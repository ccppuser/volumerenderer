#pragma once

#include "Misc.h"

// voxel 데이터로 구성된 씬
class VoxelScene
{
	struct VoxelIndex
	{
		int x;
		int y;
		int z;
	};

public:
	VoxelScene(int volumeWidth, int volumeHeight, int volumeDepth);
	~VoxelScene(void);

	// 밀도값이 들어있는 1차원 배열을 인자로 받아 voxel 데이터 구성
	bool InitializeVoxelData(unsigned char *densityData);
	// 방향성 광원 설정
	void SetDirectionalLight(const Vector3 &_direction, const Material &_material);
	// voxel 데이터에 적용할 재질 설정
	void SetVoxelMaterial(const Material &material);
	// 렌더링을 허용할 밀도값 설정
	void SetDensityBound(unsigned char low, unsigned char high);
	// voxel 데이터 크기 설정
	void SetScale(float ratio);
	// 해당 샘플링 좌표가 voxel 데이터 내에 존재하는지 검사
	bool IsInVoxelData(const Vector3 &sample);
	// 해당 샘플링 좌표를 voxel 데이터 내에서 resampling
	Color Resampling(const Ray &ray, const Vector3 &sample, Vector3 &normal);
	// 현재 씬에 설정된 재질 리턴
	Material &GetMaterial();

private:
	// 해당 인덱스의 voxel 리턴
	Voxel &GetVoxel(int x, int y, int z);
	Voxel &GetVoxel(const VoxelIndex &index);
	// 샘플링 좌표를 voxel 데이터에 매핑함
	void MappingSampledCoordinate(const Vector3 &sampledCoordinate, Vector3 &outMappedSample, Vector3 &outRoundedSample, VoxelIndex &outNearestIndex);
	// 허용 밀도 범위에 따라 불투명도를 계산(1-D Transfer function)
	Color OpacityTransfer(const Voxel &nearestVoxel);
	// Tri-linear interpolation
	void TriLinearInterpolation(const Vector3 &mappedSample, const Vector3 &roundedSample, const VoxelIndex &nearestIndex, float &outOpacity, Vector3 &outNormal);
	// 해당 법선 벡터에서의 퐁 셰이딩 계산
	Vector3 CaculatePhong(const Ray &ray, const Vector3 &normal);
	void Cleanup();

private:
	int m_nVolumeWidth;
	int m_nVolumeHeight;
	int m_nVolumeDepth;
	Voxel *m_pVoxelData;
	Vector3 m_vMinPosition;	// 가장 작은 x, y, z값의 좌표에 위치한 voxel의 좌표
	Vector3 m_vMaxPosition;	// 가장 큰 x, y, z값의 좌표에 위치한 voxel의 좌표
	Material m_mtMaterial;
	DirectionalLight m_directionalLight;
	unsigned char m_ucLowDensity;
	unsigned char m_ucHighDensity;
	float m_fScale;
};
