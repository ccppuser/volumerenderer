#pragma once

#include "VoxelScene.h"
#include <windows.h>
#include <string>

// 볼륨 렌더러
class VRenderer
{
public:
	static VRenderer *CreateVolumeRenderer(HWND hWnd);
	static VRenderer *GetInstance();
	static void DisposeVolumeRenderer();

	// 밀도값 데이터의 속성을 입력하여 초기화
	bool Initialize(const char *volumeFileName, int volumeWidth, int volumeHeight, int volumeDepth);
	// 현재 설정된 voxel 데이터 씬 리턴
	VoxelScene *GetVoxelScene() const;
	// 카메라(화면)가 원점을 기준으로한 궤도를 설정
	void SetCamera(float orbitX, float orbitY, float orbitZ, float distance);
	// 윈도우 DC에 렌더링
	bool Render(HDC hdc);

private:
	VRenderer(HWND hWnd);
	~VRenderer(void);

	void Cleanup();
	// 주어진 광선을 3D 공간에 발사하여 결과 색 리턴
	Vector3 CastRay(const Ray &ray);

private:
	static VRenderer *m_spRenderer;
	HWND m_hWnd;
	int m_nScreenWidth;
	int m_nScreenHeight;
	VoxelScene *m_pVoxelScene;
	unsigned char *m_pImage;
	Matrix m_matRotX;
	Matrix m_matRotY;
	Matrix m_matRotZ;
	float m_fDistance;
};
