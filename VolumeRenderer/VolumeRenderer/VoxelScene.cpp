#include "StdAfx.h"
#include "VoxelScene.h"
#include <exception>

VoxelScene::VoxelScene(int volumeWidth, int volumeHeight, int volumeDepth)
: m_nVolumeWidth(volumeWidth)
, m_nVolumeHeight(volumeHeight)
, m_nVolumeDepth(volumeDepth)
, m_pVoxelData(NULL)
, m_vMinPosition((float)(-volumeWidth / 2), (float)(-volumeHeight / 2), (float)(-volumeDepth / 2))
, m_vMaxPosition((float)(volumeWidth / 2), (float)(volumeHeight / 2), (float)(volumeDepth / 2))
, m_ucLowDensity(0)
, m_ucHighDensity(255)
, m_fScale(1.0f)
{
}

VoxelScene::~VoxelScene(void)
{
	this->Cleanup();
}

bool VoxelScene::InitializeVoxelData(unsigned char *densityData)
{
	try
	{
		m_pVoxelData = new Voxel[m_nVolumeWidth * m_nVolumeHeight * m_nVolumeDepth];
	}
	catch(std::bad_alloc)
	{
		MessageBox(NULL, L"메모리가 부족합니다.", L"에러", MB_OK);
		return false;
	}

	int maxIndex = (m_nVolumeWidth - 1) * (m_nVolumeHeight - 1) * (m_nVolumeDepth - 1);
#pragma omp parallel for
	for(int index = 0; index <= maxIndex; ++index)
	{
		// voxel에 밀도 복사(Data Acquisition)
		m_pVoxelData[index].density = densityData[index];
	}

#pragma omp parallel for
	for(int index = 0; index <= maxIndex; ++index)
	{
		int k = index / (m_nVolumeWidth * m_nVolumeHeight);
		int j = (index - k * (m_nVolumeWidth * m_nVolumeHeight)) / m_nVolumeWidth;
		int i = index - (k * (m_nVolumeWidth * m_nVolumeHeight) + j * m_nVolumeWidth);

		// 배열 인덱스를 초과하지 않도록 함
		if(i < 2 || i > m_nVolumeWidth - 3
			|| j < 2 || j > m_nVolumeHeight - 3
			|| k < 2 || k > m_nVolumeDepth - 3)
			continue;

		// voxel에 법선벡터(gradient vector)계산(Gradient Computation)
		int x = 0;
		int y = 0;
		int z = 0;
		x = (int)densityData[k * (m_nVolumeWidth * m_nVolumeHeight) + j * m_nVolumeWidth + (i + 1)]
			- (int)densityData[k * (m_nVolumeWidth * m_nVolumeHeight) + j * m_nVolumeWidth + i - 1];

		y = (int)densityData[k * (m_nVolumeWidth * m_nVolumeHeight) + (j + 1) * m_nVolumeWidth + i]
			- (int)densityData[k * (m_nVolumeWidth * m_nVolumeHeight) + (j - 1) * m_nVolumeWidth + i];

		z = (int)densityData[(k + 1) * (m_nVolumeWidth * m_nVolumeHeight) + j * m_nVolumeWidth + i]
			- (int)densityData[(k - 1) * (m_nVolumeWidth * m_nVolumeHeight) + j * m_nVolumeWidth + i];

		// 공식 : N(x, y, z) = df(x, y, z) / ||df(x, y, z)||
		m_pVoxelData[index].normal.Set((float)x * 0.5f, (float)y * 0.5f, (float)z * 0.5f).Normalize();

		// voxel에 재질 부여(Classification)
		m_pVoxelData[index].opacity = (float)m_pVoxelData[index].density / 255.0f;	// 불투명도(1-D Transfer Function)
	}

	return true;
}

bool VoxelScene::IsInVoxelData(const Vector3 &sample)
{
	if(sample.x / m_fScale < m_vMinPosition.x
		|| sample.y / m_fScale < m_vMinPosition.y
		|| sample.z / m_fScale < m_vMinPosition.z)
	{
		return false;
	}

	if(sample.x / m_fScale > m_vMaxPosition.x
		|| sample.y / m_fScale > m_vMaxPosition.y
		|| sample.z / m_fScale > m_vMaxPosition.z)
	{
		return false;
	}

	return true;
}

Color VoxelScene::Resampling(const Ray &ray, const Vector3 &sample, Vector3 &normal)
{
	normal.MakeZero();
	Color c;
	c.opacity = 0.0f;

	// 샘플 좌표가 voxel 데이터 내에서 어디쯤 있는지 계산
	Vector3 mappedSample;	// 매핑된 샘플링 좌표
	Vector3 roundedSample;	// 반올림된 좌표(Tri-linear보간 수행 시 소수점으로 비율을 파악하기 위해 필요)
	VoxelIndex nearestIndex;	// 해당 샘플링 좌표에 인접한 voxel의 인덱스
	this->MappingSampledCoordinate(sample, mappedSample, roundedSample, nearestIndex);

	// 배열의 범위를 초과하지 않도록 함(추가로 계산의 편의를 위해 1픽셀의 경계 인덱스 역시 무시)
	if(nearestIndex.x <= 1 || nearestIndex.x >= m_nVolumeWidth - 2
		|| nearestIndex.y <= 1 || nearestIndex.y >= m_nVolumeHeight - 2
		|| nearestIndex.z <= 1 || nearestIndex.z >= m_nVolumeDepth - 2)
		return c;

	// 허용 밀도 범위에 따라 불투명도를 계산(1-D Transfer function)
	Color transferedColor = this->OpacityTransfer(GetVoxel(nearestIndex));
	if(transferedColor.opacity == 0.0f)
		return c;

	// Tri-linear interpolation
	float opacity = 0.0f;
	this->TriLinearInterpolation(mappedSample, roundedSample, nearestIndex, opacity, normal);
	// 모든 voxel의 확산광을 동일하게 취급하므로 확산광은 보간하지 않음
	c.opacity = opacity * transferedColor.opacity;
	c.color = transferedColor.color;//m_mtMaterial.diffuse;

	// 광원에 의한 색 계산하여 누적
	c.color += this->CaculatePhong(ray, normal);

	return c;
}

void VoxelScene::SetDirectionalLight(const Vector3 &_direction, const Material &_material)
{
	m_directionalLight.direction = _direction;
	m_directionalLight.direction.Normalize();
	m_directionalLight.material = _material;
}

void VoxelScene::SetVoxelMaterial(const Material &material)
{
	m_mtMaterial = material;
}

void VoxelScene::SetDensityBound(unsigned char low, unsigned char high)
{
	m_ucLowDensity = low;
	m_ucHighDensity = high;
}

void VoxelScene::SetScale(float ratio)
{
	m_fScale = ratio;
}

Material &VoxelScene::GetMaterial()
{
	return m_mtMaterial;
}

void VoxelScene::Cleanup()
{
	if(m_pVoxelData)
	{
		delete [] m_pVoxelData;
		m_pVoxelData = NULL;
	}
}

Voxel &VoxelScene::GetVoxel(int x, int y, int z)
{
	return m_pVoxelData[z * (m_nVolumeWidth * m_nVolumeHeight) + y * (m_nVolumeWidth) + x];
}

Voxel &VoxelScene::GetVoxel(const VoxelIndex &index)
{
	return m_pVoxelData[index.z * (m_nVolumeWidth * m_nVolumeHeight) + index.y * (m_nVolumeWidth) + index.x];
}

void VoxelScene::MappingSampledCoordinate(const Vector3 &sampledCoordinate, Vector3 &outMappedSample, Vector3 &outRoundedSample, VoxelIndex &outNearestIndex)
{
	outMappedSample = sampledCoordinate / m_fScale;

	outRoundedSample.x = floorf(outMappedSample.x + 0.5f);
	outRoundedSample.y = floorf(outMappedSample.y + 0.5f);
	outRoundedSample.z = floorf(outMappedSample.z + 0.5f);

	outNearestIndex.x = (int)(outRoundedSample.x) + (m_nVolumeWidth / 2);
	outNearestIndex.y = (m_nVolumeHeight / 2) - (int)(outRoundedSample.y);
	outNearestIndex.z = (int)(outRoundedSample.z) + (m_nVolumeDepth / 2);

}

Color VoxelScene::OpacityTransfer(const Voxel &nearestVoxel)
{
	Color c;
	c.color = m_mtMaterial.diffuse;
	c.opacity = 0.0f;

	if(nearestVoxel.density < m_ucLowDensity || nearestVoxel.density > m_ucHighDensity)
		return c;

	c.opacity = (((float)nearestVoxel.density - (float)m_ucLowDensity) / ((float)m_ucHighDensity - (float)m_ucLowDensity)) * 3.0f;
	if(c.opacity > 1.0f)
	{
		c.opacity = 1.0f;
	}
	else
	{
		c.color.x *= c.opacity;
	}

	return c;
}

void VoxelScene::TriLinearInterpolation(const Vector3 &mappedSample, const Vector3 &roundedSample, const VoxelIndex &nearestIndex, float &outOpacity, Vector3 &outNormal)
{
	const int &x = nearestIndex.x;
	const int &y = nearestIndex.y;
	const int &z = nearestIndex.z;

	// ratio1, 2는 더하는 두 voxel 사이의 비율
	float ratio1 = mappedSample.x - (roundedSample.x - 1.0f);
	float ratio2 = roundedSample.x - mappedSample.x;
	outOpacity += GetVoxel(x, y, z).opacity * ratio1
		+ GetVoxel(x - 1, y, z).opacity * ratio2
		+ GetVoxel(x, y - 1, z).opacity * ratio1
		+ GetVoxel(x - 1, y - 1, z).opacity * ratio2
		+ GetVoxel(x, y, z + 1).opacity * ratio1
		+ GetVoxel(x - 1, y, z + 1).opacity * ratio2
		+ GetVoxel(x, y - 1, z + 1).opacity * ratio1
		+ GetVoxel(x - 1, y - 1, z + 1).opacity * ratio2;

	outNormal += GetVoxel(x, y, z).normal * ratio1
		+ GetVoxel(x - 1, y, z).normal * ratio2
		+ GetVoxel(x, y - 1, z).normal * ratio1
		+ GetVoxel(x - 1, y - 1, z).normal * ratio2
		+ GetVoxel(x, y, z + 1).normal * ratio1
		+ GetVoxel(x - 1, y, z + 1).normal * ratio2
		+ GetVoxel(x, y - 1, z + 1).normal * ratio1
		+ GetVoxel(x - 1, y - 1, z + 1).normal * ratio2;

	ratio1 = mappedSample.y - (roundedSample.y - 1.0f);
	ratio2 = roundedSample.y - mappedSample.y;
	outOpacity += GetVoxel(x, y, z).opacity * ratio1
		+ GetVoxel(x, y - 1, z).opacity * ratio2
		+ GetVoxel(x - 1, y, z).opacity * ratio1
		+ GetVoxel(x - 1, y - 1, z).opacity * ratio2
		+ GetVoxel(x, y, z + 1).opacity * ratio1
		+ GetVoxel(x, y - 1, z + 1).opacity * ratio2
		+ GetVoxel(x - 1, y, z + 1).opacity * ratio1
		+ GetVoxel(x - 1, y - 1, z + 1).opacity * ratio2;

	outNormal += GetVoxel(x, y, z).normal * ratio1
		+ GetVoxel(x, y - 1, z).normal * ratio2
		+ GetVoxel(x - 1, y, z).normal * ratio1
		+ GetVoxel(x - 1, y - 1, z).normal * ratio2
		+ GetVoxel(x, y, z + 1).normal * ratio1
		+ GetVoxel(x, y - 1, z + 1).normal * ratio2
		+ GetVoxel(x - 1, y, z + 1).normal * ratio1
		+ GetVoxel(x - 1, y - 1, z + 1).normal * ratio2;

	ratio1 = mappedSample.z - (roundedSample.z - 1.0f);
	ratio2 = roundedSample.z - mappedSample.z;
	outOpacity += GetVoxel(x, y, z).opacity * ratio1
		+ GetVoxel(x, y, z + 1).opacity * ratio2
		+ GetVoxel(x, y - 1, z).opacity * ratio1
		+ GetVoxel(x, y - 1, z + 1).opacity * ratio2
		+ GetVoxel(x - 1, y, z).opacity * ratio1
		+ GetVoxel(x - 1, y, z + 1).opacity * ratio2
		+ GetVoxel(x - 1, y - 1, z).opacity * ratio1
		+ GetVoxel(x - 1, y - 1, z + 1).opacity * ratio2;

	outNormal += GetVoxel(x, y, z).normal * ratio1
		+ GetVoxel(x, y, z + 1).normal * ratio2
		+ GetVoxel(x, y - 1, z).normal * ratio1
		+ GetVoxel(x, y - 1, z + 1).normal * ratio2
		+ GetVoxel(x - 1, y, z).normal * ratio1
		+ GetVoxel(x - 1, y, z + 1).normal * ratio2
		+ GetVoxel(x - 1, y - 1, z).normal * ratio1
		+ GetVoxel(x - 1, y - 1, z + 1).normal * ratio2;

	// 누적된 불투명도와 법선 벡터를 평균냄
	outOpacity = outOpacity / 12.0f;
	outNormal.Normalize();
}

Vector3 VoxelScene::CaculatePhong(const Ray &ray, const Vector3 &normal)
{
	Vector3 C;

	Vector3 L = m_directionalLight.direction;	// 광원으로부터 오는 벡터
	Vector3 N(normal);	// 법선 벡터
	Vector3 V(ray.direction);	// 화면으로부터 오는 벡터
	Vector3 H = (V + L).Normalize();	// Halfway 벡터
	float theta = N.Dot(L);	// N과 L 사이 각
	float beta = N.Dot(H);	// N과 H 사이 각

	// add diffuse
	if(theta > 0.0f)
	{
		// I += Ka * Ia + Kd * Id * DOT(N, L)
		C += m_mtMaterial.ambient * m_directionalLight.material.ambient
			+ m_mtMaterial.diffuse * m_directionalLight.material.diffuse * theta;
	}
	// add specular
	if(beta > 0.0f)
	{
		// I += Ks * Is * DOT(N, H)^ns
		C += m_mtMaterial.specular * m_directionalLight.material.specular * powf(beta, 0.5f);
	}

	return C;
}
